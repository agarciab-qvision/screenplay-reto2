package co.com.proyectobase.screenplay.stepdefinitions;

import cucumber.api.DataTable;
import static org.hamcrest.Matchers.equalTo;
import java.util.List;
import static net.serenitybdd.screenplay.GivenWhenThen.seeThat;
import org.openqa.selenium.WebDriver;
import co.com.proyectobase.screenplay.questions.LaPantalla;
import co.com.proyectobase.screenplay.tasks.Abrir;
import co.com.proyectobase.screenplay.tasks.Diligenciar;
import cucumber.api.java.Before;
import cucumber.api.java.ast.Cuando;
import cucumber.api.java.es.Dado;
import cucumber.api.java.es.Entonces;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.abilities.BrowseTheWeb;
import net.thucydides.core.annotations.Managed;

public class WebOrangeHRMEnterpriseStepDefinitions {

	@Managed(driver="chrome")
    private WebDriver hisBrowser;
    private Actor juan = Actor.named("Juan");
    
    @Before
    public void configuracionInicial() {
    	juan.can(BrowseTheWeb.with(hisBrowser));
    }

    @Dado("^que Juan necesita crear un empleado en el OrangeHR$")
    public void queJuanNecesitaCrearUnEmpleadoEnElOrangeHR()  {
    	juan.wasAbleTo(Abrir.LaPaginaDeHRMEnterprise());
    }

    @Cuando("^el realiza el ingreso del registro en la aplicación$")
    public void elRealizaElIngresoDelRegistroEnLaAplicación(DataTable dtDatosFormulario)  {
    	
    	List<List<String>> datos = dtDatosFormulario.raw();
    	juan.attemptsTo(Diligenciar.FormularioDeRegistro(datos));
    }

    @Entonces("^el visualiza el nuevo empleado en el aplicativo$")
    public void elVisualizaElNuevoEmpleadoEnElAplicativo()  {
    	juan.should(seeThat(LaPantalla.muestra(),equalTo("0123")));
    }
    
}
