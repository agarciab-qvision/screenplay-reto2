#Author: agarciab@choucairtesting.com
#language:es

@tag
Característica: Automation Demo Site
  Como usuario
  Quiero ingresar al sitio Web Automation Demo Site
  A registrar mi usuario

  @tag1
  Escenario: Registrar usuario
    Dado que Juan necesita crear un empleado en el OrangeHR
    Cuando el realiza el ingreso del registro en la aplicación
    |First Name|Middle Name|Last Name			|Eployee ID	|Location								|Date Birth|Marital Status|Gender	|Nationality|NickName|License Expiry|Blood group|Hobbies|
    |Alexander |Steven 		 |García Berrío |0123				|Australian Regional HQ |1978-10-09|Married				|Male		|Colombian	|asgarcia|2040-10-09		|O					|Music	|
    Entonces el visualiza el nuevo empleado en el aplicativo