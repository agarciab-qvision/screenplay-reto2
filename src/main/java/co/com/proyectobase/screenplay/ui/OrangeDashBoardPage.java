package co.com.proyectobase.screenplay.ui;

import net.serenitybdd.core.annotations.findby.By;
import net.serenitybdd.screenplay.targets.Target;

public class OrangeDashBoardPage {
	
	public static final Target BOTON_PIM = Target.the("El botón de PIM").located(By.id("menu_pim_viewPimModule"));
	public static final Target BOTON_ADD_EMPLOYEE = Target.the("El botón para adicionar empleado").located(By.id("menu_pim_addEmployee"));
	public static final Target BOTON_EMPLOYEE_LIST = Target.the("El botón lista de empleados").located(By.id("menu_pim_viewEmployeeList"));
	public static final Target CAMPO_FIRST_NAME = Target.the("El campo primer nombre").located(By.id("firstName"));
	public static final Target CAMPO_MIDDLE_NAME = Target.the("El campo segundo nombre").located(By.id("middleName"));
	public static final Target CAMPO_LAST_NAME = Target.the("El campo apellidos").located(By.id("lastName"));
	public static final Target CAMPO_EMPLOYEE_ID = Target.the("El ID del empleado").located(By.id("employeeId"));
	public static final Target CAMPO_LOCATION = Target.the("La ubicación").located(By.id("location_inputfileddiv"));
	public static final Target LISTA_LOCATION = Target.the("La lista de ubicación").located(By.id("location_inputfileddiv"));
	public static final Target BOTON_SAVE = Target.the("El botón de Guardar").located(By.id("systemUserSaveBtn"));
	public static final Target BOTON_SAVE_PERSONAL_DETAILS = Target.the("El botón de Guardar detalles personales").located(By.id("pimPersonalDetailsForm"));
	
}
