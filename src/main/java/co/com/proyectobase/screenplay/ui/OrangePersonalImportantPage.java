package co.com.proyectobase.screenplay.ui;

import net.serenitybdd.core.annotations.findby.By;

import net.serenitybdd.screenplay.targets.Target;



public class OrangePersonalImportantPage {

	
	public static final Target CAMPO_BIRTH_DAY = Target.the("El campo fecha de nacimiento").located(By.id("date_of_birth"));
	public static final Target CAMPO_MARITAL_STATUS = Target.the("El estado civil").located(By.id("marital_status_inputfileddiv"));
	public static final Target LISTA_ESTADO_CIVIL = Target.the("El estado civil").located(By.id("marital_status"));
	public static final Target CHECK_GENDER_MALE = Target.the("El genero masculino").located(By.id("gender_1"));
	public static final Target LISTA_NACIONALIDAD = Target.the("La nacionalidad").located(By.id("nationality_inputfileddiv"));
	public static final Target CAMPO_NICKNAME = Target.the("El NickName").located(By.id("nickName"));
	public static final Target CAMPO_LICENCIA_CONDUCCION = Target.the("Numero licencia de conducción").located(By.id("driver_license"));
	public static final Target CAMPO_FECHA_EXPIRACION_LICENCIA = Target.the("Fecha expiracion licencia de conduccion").located(By.id("license_expiry_date"));
	public static final Target LISTA_TIPO_SANGRE = Target.the("Tipo de sangre").located(By.id("1_inputfileddiv"));
	public static final Target CAMPO_HOBBIES = Target.the("Hobbies").located(By.id("5"));
	public static final Target BOTON_SAVE_PERSONAL_DETAILS = Target.the("El botón de Guardar detalles personales").located(By.xpath("//*[@id=\'content\']/div[2]/ui-view/div[2]/div/custom-fields-panel/div/form/materializecss-decorator[3]/div/sf-decorator/div/button"));
	
}
