package co.com.proyectobase.screenplay.ui;

import net.serenitybdd.core.annotations.findby.By;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.screenplay.targets.Target;
import net.thucydides.core.annotations.DefaultUrl;


@DefaultUrl("https://orangehrm-demo-6x.orangehrmlive.com/")


public class OrangeAutenticationPage extends PageObject{

	public static final Target BOTON_LOGIN = Target.the("El botón de Login").located(By.id("btnLogin"));

	
	
}
