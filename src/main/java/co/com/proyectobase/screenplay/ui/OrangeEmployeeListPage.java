package co.com.proyectobase.screenplay.ui;

import net.serenitybdd.core.annotations.findby.By;
import net.serenitybdd.screenplay.targets.Target;

public class OrangeEmployeeListPage {

    public static final Target CAMPO_BUSQUEDA_NOMBRE = Target.the("El campo del nombre a buscar").located(By.id("employee_name_quick_filter_employee_list_value"));
    public static final Target BOTON_BUSQUEDA = Target.the("El botón de búsqueda").located(By.id("quick_search_icon"));
    public static final Target TABLA_RESULTADO = Target.the("Tabla de resultados").located(By.xpath("//*[@id='employeeListTable']/tbody/tr/td[3]"));
    public static final Target CAMPO_VERIFICACION = Target.the("Campo de verificacion").located(By.id("angucomplete-title-temp-id"));
		
}
