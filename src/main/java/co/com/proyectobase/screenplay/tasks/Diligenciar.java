package co.com.proyectobase.screenplay.tasks;



import java.util.List;

import org.openqa.selenium.JavascriptExecutor;

import co.com.proyectobase.screenplay.interactions.Seleccionar;
import co.com.proyectobase.screenplay.ui.OrangeDashBoardPage;
import co.com.proyectobase.screenplay.ui.OrangeEmployeeListPage;
import co.com.proyectobase.screenplay.ui.OrangePersonalImportantPage;
import net.serenitybdd.core.Serenity;
import net.serenitybdd.core.annotations.findby.By;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;



public class Diligenciar implements Task {
	
	private List<List<String>> datos;
	
	@Override
	public <T extends Actor> void performAs(T actor) {
		
		actor.attemptsTo(Click.on(OrangeDashBoardPage.BOTON_PIM));
		actor.attemptsTo(Click.on(OrangeDashBoardPage.BOTON_ADD_EMPLOYEE));

		actor.attemptsTo(Enter.theValue(datos.get(1).get(0).trim()).into(OrangeDashBoardPage.CAMPO_FIRST_NAME));
		actor.attemptsTo(Enter.theValue(datos.get(1).get(1).trim()).into(OrangeDashBoardPage.CAMPO_MIDDLE_NAME));
		actor.attemptsTo(Enter.theValue(datos.get(1).get(2).trim()).into(OrangeDashBoardPage.CAMPO_LAST_NAME));
		actor.attemptsTo(Enter.theValue(datos.get(1).get(3).trim()).into(OrangeDashBoardPage.CAMPO_EMPLOYEE_ID));
		actor.attemptsTo(Click.on(OrangeDashBoardPage.CAMPO_LOCATION));
		actor.attemptsTo(Seleccionar.laLista(OrangeDashBoardPage.LISTA_LOCATION, datos.get(1).get(4).trim()));
		actor.attemptsTo(Click.on(OrangeDashBoardPage.BOTON_SAVE));
		
		try {
			Thread.sleep(6000);
		} catch (InterruptedException e) {
			// 
			e.printStackTrace();
		}
		

		actor.attemptsTo(Enter.theValue(datos.get(1).get(5).trim()).into(OrangePersonalImportantPage.CAMPO_BIRTH_DAY));
		actor.attemptsTo(Click.on(OrangePersonalImportantPage.CAMPO_MARITAL_STATUS));
		actor.attemptsTo(Seleccionar.laLista(OrangePersonalImportantPage.CAMPO_MARITAL_STATUS, datos.get(1).get(6).trim()));
		JavascriptExecutor exe = (JavascriptExecutor)Serenity.getWebdriverManager().getCurrentDriver();
		exe.executeScript("arguments[0].click()", Serenity.getWebdriverManager().getCurrentDriver().findElement(By.xpath("//*[@for='gender_1']")));
		actor.attemptsTo(Click.on(OrangePersonalImportantPage.LISTA_NACIONALIDAD));
		actor.attemptsTo(Seleccionar.laLista(OrangePersonalImportantPage.LISTA_NACIONALIDAD, datos.get(1).get(8).trim()));
		actor.attemptsTo(Enter.theValue(datos.get(1).get(9).trim()).into(OrangePersonalImportantPage.CAMPO_NICKNAME));
		actor.attemptsTo(Enter.theValue(datos.get(1).get(10).trim()).into(OrangePersonalImportantPage.CAMPO_FECHA_EXPIRACION_LICENCIA));
		actor.attemptsTo(Click.on(OrangePersonalImportantPage.LISTA_TIPO_SANGRE));

		actor.attemptsTo(Seleccionar.laLista(OrangePersonalImportantPage.LISTA_TIPO_SANGRE, datos.get(1).get(11).trim()));
		actor.attemptsTo(Enter.theValue(datos.get(1).get(12).trim()).into(OrangePersonalImportantPage.CAMPO_HOBBIES));
		actor.attemptsTo(Click.on(OrangePersonalImportantPage.BOTON_SAVE_PERSONAL_DETAILS));
		
		try {
			Thread.sleep(6000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
		actor.attemptsTo(Click.on(OrangeDashBoardPage.BOTON_EMPLOYEE_LIST));
		actor.attemptsTo(Enter.theValue(datos.get(1).get(3).trim()).into(OrangeEmployeeListPage.CAMPO_BUSQUEDA_NOMBRE));
		
		try {
			Thread.sleep(6000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
	
	

	public Diligenciar(List<List<String>> datos) {
		this.datos = datos;
	}


	public static Diligenciar FormularioDeRegistro(List<List<String>> datos) {
		return Tasks.instrumented(Diligenciar.class, datos);
	}

}
