package co.com.proyectobase.screenplay.tasks;

import co.com.proyectobase.screenplay.ui.OrangeAutenticationPage;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Open;

public class Abrir implements Task {

	private OrangeAutenticationPage webOrangeHRMEnterprisePage;
	
	@Override
	public <T extends Actor> void performAs(T actor) {
		actor.attemptsTo(Open.browserOn(webOrangeHRMEnterprisePage));
		actor.attemptsTo(Click.on(OrangeAutenticationPage.BOTON_LOGIN));
	}
	
	public static Abrir LaPaginaDeHRMEnterprise() {
		
		return Tasks.instrumented(Abrir.class);
	}

}

